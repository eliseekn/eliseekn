Hello 👋, my name is Elisée K. N'Guessan,
=
I'm a self-taught **Full-stack developer** with **4+ years experience**.

*   🌍  I'm based in Abidjan (Côte d'Ivoire)
*   💻  See my portfolio at [eliseekn.netlify.app](https://eliseekn.netlify.app)
*   📧  You can contact me at [eliseekn@gmail.com](mailto:eliseekn@gmail.com)

![Alt text](https://www.codewars.com/users/eliseekn/badges/small "CodeWars")

#### :fire: I work with
<p>
  <a target="_blank" href="#"><img src="https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge" /></a>&nbsp;
  <a target="_blank" href="#"><img src="https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge" /></a>&nbsp;
  <a target="_blank" href="#"><img src="https://img.shields.io/badge/React-20232A?style=for-the-badge" /></a>&nbsp;
  <a target="_blank" href="#"><img src="https://img.shields.io/badge/Vue%20js-35495E?style=for-the-badge" /></a>&nbsp;
  <a target="_blank" href="#"><img src="https://img.shields.io/badge/React_Native-20232A?style=for-the-badge" /></a>&nbsp;
  <a target="_blank" href="#"><img src="https://img.shields.io/badge/next%20js-000000?style=for-the-badge" /></a>&nbsp;
  <a target="_blank" href="#"><img src="https://img.shields.io/badge/MySQL-005C84?style=for-the-badge" /></a>&nbsp;
  <a target="_blank" href="#"><img src="https://img.shields.io/badge/Prisma-3982CE?style=for-the-badge" /></a>&nbsp;
  <a target="_blank" href="#"><img src="https://img.shields.io/badge/Linux-FCC624?style=for-the-badge" /></a>&nbsp;
  <a target="_blank" href="#"><img src="https://img.shields.io/badge/GIT-E44C30?style=for-the-badge" /></a>&nbsp;
</p>
